﻿#pragma strict

public var scene = "";
public var position : Vector3;
public var rotation : Vector3;

function OnInteract(){
	if (scene != ""){
		Shared.Pause();
		f00.ScreenFade(f00.SOLID, gameObject);
	}
}

function OnFinishedFading(){
	Shared.set_player = true;
	Shared.player_position = position;
	Shared.player_rotation = rotation;
	Application.LoadLevel(scene);
}
