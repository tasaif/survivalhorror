﻿#pragma strict

var player_handle : GameObject;
var focal_point : Transform;
var cam_system : GameObject;

function Start () {
	player_handle = GameObject.Find("Player");
	cam_system = transform.parent.gameObject;
	focal_point = player_handle.transform.FindChild("focal_point").transform;
}

function Update () {
	transform.LookAt(focal_point);
}