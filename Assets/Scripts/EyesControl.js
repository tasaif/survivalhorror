﻿#pragma strict

var sight_distance = 4;
var grab_distance = .1f;

function can_see(){
	var hitinfo : RaycastHit;
	var direction = transform.TransformDirection(Vector3.forward);
	Debug.DrawRay(gameObject.transform.position, direction * sight_distance, Color.red);
	if (Physics.Raycast(gameObject.transform.position,  direction, hitinfo, sight_distance, 1 << 13)){
		return true;
	}
	return false;
}

function can_grab(){
	var hitinfo : RaycastHit;
	var direction = transform.TransformDirection(Vector3.forward);
	Debug.DrawRay(gameObject.transform.position, direction * grab_distance, Color.green);
	if (Physics.Raycast(gameObject.transform.position,  direction, hitinfo, grab_distance, 1 << 13)){
		return true;
	}
	return false;
}