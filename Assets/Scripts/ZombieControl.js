﻿#pragma strict

var player : GameObject;
var eyes : EyesControl;
var animator : Animator;
var walk_movement_speed = .4f;
var rotate_speed = 2;
var anim_speed = .5f;

var IDLE = 0;
var HUNT = 1;
var ATTACK = 2;
var TURNING = 3;
var state = IDLE;

function Start () {
	 animator = GetComponentInChildren.<Animator>();
	 eyes = gameObject.transform.FindChild('eyes').GetComponent.<EyesControl>();
	 player = f00.find('Player');
}

function handlePauseState(){
	if (Shared.paused > 0){
		animator.speed = 0;
		return true;
	}
	animator.speed = 1;
	return false;
}

function Update () {
	if (handlePauseState()) return;
	switch(state){
	case IDLE:
		if (eyes.can_see()){
			state = HUNT;
			animator.SetInteger("State", HUNT);
		}
		break;
	case HUNT:
		gameObject.transform.LookAt(player.transform);
		f00.tryTranslate (gameObject, Vector3.forward * (walk_movement_speed) * Time.deltaTime);
		if (eyes.can_grab()){
			state = ATTACK;
			animator.SetInteger("State", IDLE);
		}
		break;
	}
}