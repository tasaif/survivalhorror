﻿#pragma strict

var player_transform : Transform;

function Start () {
	var taken_items : Hashtable = Shared.takenItems();
	var i = 0;
	for (var item : ItemControl in f00.find('items').GetComponentsInChildren.<ItemControl>()){
		item.id = i;
		item.original_quantity = item.quantity;
		if (taken_items[i] != null){
			var taken_amount : int = taken_items[i];
			item.quantity -= taken_amount;
		}
		if (item.quantity == 0) item.gameObject.SetActive(false);
		i++;
	}
	f00.ScreenFade(f00.TRANSPARENT);
	Shared.paused = 0;
	player_transform = GameObject.Find("Player").transform;
	if (Shared.set_player){
		player_transform.position = Shared.player_position;
		player_transform.localEulerAngles = Shared.player_rotation;
	}
}

function Update () {
	//if (f00.getKeyDown(KeyCode.Space)){
	//	f00.TogglePause();
	//}
	if (f00.getKeyDown(KeyCode.I)){
		if (!f00.find('Inventory'))
			var instance : GameObject = Instantiate(Resources.Load("Inventory/Inventory", GameObject));	
	}
}
