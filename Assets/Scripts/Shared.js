﻿#pragma strict

static var set_player = false;
static var player_position : Vector3;
static var player_rotation : Vector3;
static var inventory = new Array();
static var inventory_cap = 4;
static var room_items = new Hashtable();
static var taken_items = new Hashtable(); //Holds the quantities of each item taken
static var paused = 0;

static var FAILED = 0;
static var SUCCESS = 1;
static var PARTIAL = 2;
static var YES = 0;
static var NO = 1;

static function unPause(){
	return paused--;
}

static function Pause(){
	return paused++;
}

static function showMessage(text){
	var instance : MessageControl = Instantiate(Resources.Load("Message", GameObject)).GetComponent.<MessageControl>();
	instance.setMessage(text);
}

static function takenItems(takenItems){
	taken_items[Application.loadedLevelName] = takenItems;
}

static function takenItems(){
	if (taken_items[Application.loadedLevelName] == null)
		taken_items[Application.loadedLevelName] = new Hashtable();
	return taken_items[Application.loadedLevelName];
}

static function printInv(){
	for(var i=0; i<inventory.length; i++){
		var cur_item : Hashtable = inventory[i];
		f00.log(cur_item['name'] + ': ' + cur_item['quantity']);
	}
}

static function AddItem(item : GameObject) {
	AddItem(item.GetComponent.<ItemControl>());
}

static function AddItem(item : ItemControl) {
	if (item.combinable){
		var i : int = 0;
		for (var inv_item : Hashtable in inventory){
			if (inv_item['item_name'] != item.item_name){
				i++;
				continue;
			}
			var inv_item_quantity : int = inv_item['quantity'];
			inv_item['quantity'] = item.quantity + inv_item_quantity;
			item.quantity = 0;
			inventory[i] = inv_item;
			return;
		}
	}
	if (inventory.length < inventory_cap){
		inventory.push(item.toInvObject());
		item.quantity = 0;
	} else {
		showMessage('Your inventory is full');
	}
}