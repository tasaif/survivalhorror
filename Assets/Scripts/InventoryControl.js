﻿#pragma strict

var inv_items : Transform;
var selected : int = 0;
var selected_option : int = 0;
var totalItems : int;
var selector_obj : GameObject;
var option_selector_obj : GameObject;
var options : GameObject;
var select_option = false;

var USE = 0;
var EQUIP = 1;
var INSPECT = 2;
var selection_functions = [UseItem.Use, UseItem.Equip, UseItem.Inspect];

function moveSelector(position : int){
	selector_obj.transform.localPosition = new Vector3(0, -1 * (position*30), 0);
}

function moveOptionSelector(position : int){
	option_selector_obj.transform.localPosition = new Vector3(0, -1 * (position*30), 0);
}

function Start () {
	Shared.Pause();
	gameObject.name = 'Inventory';
	//Shared.AddItem(f00.find('Knife'));
	Shared.AddItem(f00.find('Pistol'));
	//Shared.AddItem(f00.find('Hacksaw'));
	//Shared.AddItem(f00.find('Ammo9mm'));
	var instance : GameObject;
	inv_items = gameObject.transform.FindChild('inventory_items');
	selector_obj = gameObject.transform.FindChild('primary_selector').FindChild('select_box').gameObject;
	options = gameObject.transform.FindChild('options').gameObject;
	option_selector_obj = options.transform.FindChild('option_select_box').gameObject;
	options.SetActive(false);
	totalItems = Shared.inventory.length;
	selector_obj.SetActive(totalItems > 0);
	gameObject.transform.FindChild('empty_inventory').gameObject.SetActive(totalItems == 0);
	gameObject.transform.FindChild('capacity').GetComponent.<UnityEngine.UI.Text>().text = String.Format("({0}/{1})", totalItems, Shared.inventory_cap);
	var i = 0;
	for (var item : Hashtable in Shared.inventory){
		instance = Instantiate(Resources.Load("Inventory/InventoryItem", GameObject));
		instance.GetComponent.<UnityEngine.UI.Text>().text = item['display_name'];
		if (item['combinable'])
			instance.transform.FindChild('ItemQuantity').GetComponent.<UnityEngine.UI.Text>().text = String.Format('x{0}', item['quantity']);
		instance.transform.SetParent(inv_items);
		instance.transform.localPosition = new Vector3(0, 80 - (i*30), 0);
		i++;
	}
}

function Update () {
	if (f00.getKeyDown(KeyCode.Escape) || f00.getKeyDown(KeyCode.I)){
		Destroy(gameObject);
	}
	if (totalItems > 0){
		if (!select_option){
			if (f00.getKeyDown(KeyCode.S)){
				if (selected == totalItems-1) selected = 0;
				else selected++;
				moveSelector(selected);
			}
			if (f00.getKeyDown(KeyCode.W)){
				if (selected == 0) selected = totalItems - 1;
				else selected--;
				moveSelector(selected);
			}
			if (f00.getKeyDown(KeyCode.D) || f00.getKeyDown(KeyCode.Return)){
				select_option = true;
				selector_obj.GetComponent.<Animator>().speed = 0;
				selector_obj.transform.FindChild('arrow').GetComponent.<UnityEngine.UI.Text>().text = '';
				options.SetActive(true);
			}
		} else {
			if (f00.getKeyDown(KeyCode.A)){
				select_option = false;
				selector_obj.GetComponent.<Animator>().speed = 1;
				selector_obj.transform.FindChild('arrow').GetComponent.<UnityEngine.UI.Text>().text = '';
				options.SetActive(false);
			}
			if (f00.getKeyDown(KeyCode.S)){
				if (selected_option == 2) selected_option = 0;
				else selected_option++;
				moveOptionSelector(selected_option);
			}
			if (f00.getKeyDown(KeyCode.W)){
				if (selected_option == 0) selected_option = 2;
				else selected_option--;
				moveOptionSelector(selected_option);
			}
			if (f00.getKeyDown(KeyCode.Return)){
				Destroy(gameObject);
				selection_functions[selected_option](Shared.inventory[selected]);
			}
		}
	}
}

function OnDestroy(){
	Shared.unPause();
}
