﻿#pragma strict

var item_name;
public var display_name = "";
public var description = "";
public var quantity : int = 0;
public var combinable : boolean = false;
public var original_quantity : int;
var id;

function Start(){
	item_name = gameObject.name;
}

function OnInteract(){
	var message : String;
	if (combinable) message = String.Format("Pick up: {1} ({0})", quantity, display_name);
	else message = String.Format("Pick up: {0}", display_name);
	var instance : PromptControl = Instantiate(Resources.Load("Prompt", GameObject)).GetComponent.<PromptControl>();
	instance.caller = transform;
	instance.setPrompt(message);
}

function toInvObject(){
	var retval = new Hashtable();
	retval['item_name'] = item_name;
	retval['display_name'] = display_name;
	retval['quantity'] = quantity;
	retval['combinable'] = combinable;
	retval['original_quantity'] = original_quantity;
	return retval;
}

function onPromptResult(result){
	if (result == Shared.YES){
		Shared.AddItem(this);
		var taken_items : Hashtable  = Shared.takenItems();
		taken_items[id] = original_quantity - quantity;
		Shared.takenItems(taken_items);
		if (quantity == 0) gameObject.SetActive(false);
		gameObject.SetActive(quantity != 0);
	}
}