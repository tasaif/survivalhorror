﻿#pragma strict

var queued : Array = new Array();

function Start () {
}

function Update () {
	if (Shared.paused) return;
	var i = 0;
	for (var t : Transform in queued){
		if (f00.getKeyDown(KeyCode.Return)){
			t.SendMessage("OnInteract");
			queued.RemoveAt(i);
			break;
		}
		i++;
	}
}

function OnTriggerEnter(other : Collider){
	queued.push(other.transform);
}

function OnTriggerExit(other : Collider){
	var i = 0;
	var found : boolean = false;
	for(var t : Transform in queued){
		if (t == other.transform){
			found = true;
			break;
		}
		i++;
	}
	if (found) queued.RemoveAt(i);
}