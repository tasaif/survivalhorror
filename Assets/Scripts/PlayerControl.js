﻿#pragma strict

var animator : Animator;
var run_movement_speed = 2f;
var walk_movement_speed = 1f;
var back_movement_speed = .5f;
var rotate_speed = 2;
var anim_speed = .5f;

var current_speed = 0f;
var running = false;
var walking = false;
var backwards = false;
var aiming = false;
var left = false;
var right = false;
var equipped = true;

function Start () {
	 animator = GetComponentInChildren.<Animator>();
}

function handlePauseState(){
	if (Shared.paused > 0){
		animator.speed = 0;
		return true;
	}
	animator.speed = 1;
	return false;
}

function Update () {
	if (handlePauseState()) return;
	walking = f00.getKey(KeyCode.W);
	running = walking && f00.getKey(KeyCode.LeftShift);
	backwards = f00.getKey(KeyCode.S);
	left = f00.getKey(KeyCode.A);
	right = f00.getKey(KeyCode.D);
	aiming = f00.getKey(KeyCode.Q);
	
	aiming = aiming && equipped;
	if (left){
		f00.Rotate(gameObject, Vector3(0, rotate_speed * -1 * Time.deltaTime, 0));
	} else if (right){
		f00.Rotate(gameObject, Vector3(0, rotate_speed * Time.deltaTime, 0));
	}
	if (!aiming){
		if (walking) {
			animator.SetInteger("State", running ? 2 : 1);
			current_speed = running ? 2f : 1f;
			f00.tryTranslate (gameObject, Vector3.forward * (current_speed) * Time.deltaTime);
		} else if (backwards) {
			 f00.tryTranslate (gameObject, Vector3.back * back_movement_speed * Time.deltaTime);
			 animator.SetInteger("State", 0);
			 animator.SetInteger("State", -1);
		}
	} else {
		animator.SetInteger("State", 3);
		f00.log('boop');
	}
	if (!walking && !running && !backwards) {
		current_speed = 0;
		animator.SetInteger("State", 0);
	}
}