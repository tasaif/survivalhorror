﻿#pragma strict

var selected = 0;
var max_option = 1;
var caller : Transform;

function setSelected(selection){
	var options_container = gameObject.transform.FindChild("options");
	var options = options_container.GetComponentsInChildren.<UI.Text>();
	for (var text : UI.Text in options){
		text.color = Color.white;
	}
	options[selection].color = Color.red;
}

function setPrompt(text){
	gameObject.transform.FindChild("prompt").GetComponentInChildren.<UI.Text>().text = text;
}

function Start () {
	Shared.Pause();
	setSelected(selected);
}

function Update () {
	if (f00.getKeyDown(KeyCode.D) && selected < max_option){
		selected++;
		setSelected(selected);
	}
	if (f00.getKeyDown(KeyCode.A) && selected > 0){
		selected--;
		setSelected(selected);
	}
	if (f00.getKeyDown(KeyCode.Return)){
		Destroy(gameObject);
	}
}

function OnDestroy(){
	Shared.unPause();
	caller.gameObject.SendMessage("onPromptResult", selected);
}