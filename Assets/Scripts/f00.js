﻿#pragma strict

static var TRANSPARENT : int = 0;
static var SOLID : int = 1;

static function is_valid_position(potv : Vector3) : Array {
	var hitinfo : RaycastHit;
	potv.y += .1;
	if (Physics.Raycast(potv, Vector3.down, hitinfo, .2, 1 << 8)){
		return [true, hitinfo.point.y];
	} else {
		return [false];
	}	
}

static function tryTranslate(that : GameObject, v){
	var old_pos = that.transform.position;
	that.transform.Translate(v);
	var new_pos = that.transform.position;
	that.transform.position = old_pos;
	var maybe_positions = [
		new_pos,
		Vector3(new_pos.x, new_pos.y, old_pos.z),
		Vector3(old_pos.x, new_pos.y, new_pos.z)
	];
	for (position in maybe_positions){
		var result = is_valid_position(position);
		if (result[0]){
			position.y = result[1];
			that.transform.position = position;
			break;
		}
	}
}

static function find(name) : GameObject {
	return GameObject.Find(name);
}

static function ScreenFade(solid){
	ScreenFade(solid, null);
}

static function ScreenFade(solid, from){
	var control = GameObject.Find("Fader").GetComponent.<FaderControl>();
	control.solid = solid;
	control.caller = from;
}

static function TurnOffCameras(){
	for (var camera : Camera in Camera.allCameras){
		camera.gameObject.SetActive(false);
	}
}

static function Move(obj : GameObject, deltas : Vector3) {
	obj.transform.localPosition = obj.transform.localPosition + deltas;
};

static function Rotate(obj : GameObject, deltas : Vector3) {
	obj.transform.localEulerAngles = obj.transform.localEulerAngles + deltas;
};

static function getKey(key : KeyCode){
	return Input.GetKey(key);
}

static function getKeyDown(key : KeyCode){
	return Input.GetKeyDown(key);
}

static function log(obj){
	Debug.Log(obj);
}

static function log_hash(obj : Hashtable){
	for(var key in obj.Keys){
		log(String.Format("{0}: {1}", key, obj[key]));
	}
}

static function hash_to_string(obj : Hashtable){
	var retval = "";
	for(var key in obj.Keys){
		retval = retval + String.Format("{0}: {1}; ", key, obj[key]);
	}
	return retval;
}

static function log_arr(obj : Array){
	for(var i=0; i<obj.length; i++){
		var val = obj[i];
		if (typeof(val) == Hashtable) val = hash_to_string(val);
		log(String.Format("{0}: {1}", i, val));
	}
}

static function Prompt(text){	
}