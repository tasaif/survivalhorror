﻿#pragma strict

var caller : GameObject;
var solid : int = 0;

function FinishedFading(){
	if (caller) caller.SendMessage("OnFinishedFading");
}

function Update(){
	GetComponent.<Animator>().SetInteger("solid", solid);
}